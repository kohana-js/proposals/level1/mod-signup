# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### accept tags: Added, Changed, Removed, Deprecated, Removed, Fixed, Security

## [5.1.0] - 2021-09-14
### Added
- allow root username configuration

## [5.0.2] - 2021-09-08
### Fixed
- fix tests
- use KohanaJS 6.0.0 and @kohanajs/mod-form

## [5.0.1] - 2021-09-07
### Change
- Model User.username change to User.name

### Added
- create CHANGELOG.md
