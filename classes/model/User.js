const {ORM} = require('kohanajs');

class User extends ORM{
  person_id = null;
  name = null;

  static joinTablePrefix = 'user';
  static tableName = 'users';

  static fields = new Map([
    ["name", "String!"]
  ]);
  static belongsTo = new Map([
    ["person_id", "Person"]
  ]);
  static hasMany = [
    ["user_id", "PasswordIdentifier"],
    ["user_id", "Login"]
  ];
  static belongsToMany = new Set([
    "Role"
  ]);
}

module.exports = User;
