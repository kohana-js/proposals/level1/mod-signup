const {build} = require('kohanajs-start');
build(
  `${__dirname}/admin.graphql`,
  `${__dirname}/admin.js`,
  `${__dirname}/exports/admin.sql`,
  `${__dirname}/../db/admin.sqlite`,
  `${__dirname}/../classes/model`,
  true,
)